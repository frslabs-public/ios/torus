#
# Be sure to run `pod lib lint Torus.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Torus'
  s.version          = '9.0.2'
  s.summary          = 'Torus is used for Transaction'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/torus'
  s.license          = 'MIT'
  s.author           = { 'Ashish' => 'ashish@frslabs.com' }
  s.source           = { :http => 'https://torus-ios.repo.frslabs.space/torus-ios/9.0.2/Torus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Torus.framework'
  s.swift_version = '5.0'
end
